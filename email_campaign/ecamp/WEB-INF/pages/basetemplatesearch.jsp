<%@ page import="java.util.List" %>
<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        <c:if test="${submitme=='Y'}">
            window.opener.location.href="<%= request.getContextPath()%>/campaign/editcampaigntemplate.view?id=<c:out value='${template}'/>&campaignId=<c:out value="${campaignId}"/>";
        //window.opener.document.forms['frmTemplate'].action="<%= request.getContextPath()%>/campaign/editcampaigntemplate.view?id=<c:out value='${template}'/>&campaignId=<c:out value="${campaignId}"/>
            //window.opener.document.forms['frmTemplate'].submit();
            window.close();
        </c:if>
        function fnSelectTemplate(templateId){
            document.forms['frmTemplateSearch'].action="<%= request.getContextPath()%>/template/copytemplate.view?campaignId=<c:out value="${campaignId}"/>&templateId="+templateId;
            document.forms['frmTemplateSearch'].submit();
        }
    </script>
    <jsp:include page="/template/includes.jsp" />
</head>
<body>
    <div class="container">
        <div class="row">
            <form class="form-search" action="<%= request.getContextPath() %>/template/basesearch.view" method="GET" id="frmTemplateSearch">
                <fieldset>
                    <legend><spring:message code="label.template.searchTemplate"/></legend>
                    <input type="text" class="input-medium" placeholder="<spring:message code="label.template.name"/>" name="name">
                    <input type="hidden" name="campaignId" value="<c:out value="${campaignId}"/>">
                    <button type="submit" class="btn"><i class="icon-search"></i></button>
                </fieldset>
            </form>
        </div>
        <div class="row">
            <c:if test="${showtable=='Y'}">
                <c:choose>
                <c:when test="${empty templates}">
                    <div class="alert span6"><spring:message code="message.nodatafound"/></div>
                </c:when>
                <c:otherwise>
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th><spring:message code="label.template.name"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="template" items="${templates}">
                            <tr>
                                <td><c:out value="${template.name}"/></td>
                                <td><a href="<%= request.getContextPath()%>/template/copytemplate.view?campaignId=<c:out value="${campaignId}"/>&templateId=<c:out value="${template.id}"/>"><spring:message code="label.master.select"/></a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:otherwise>
                </c:choose>
            </c:if>
        </div>
    </div>
</body>
</html>