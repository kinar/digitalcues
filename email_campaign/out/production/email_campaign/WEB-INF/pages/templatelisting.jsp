<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <jsp:include page="/template/includes.jsp"/>

    <%
        TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
        if(properties==null)
            properties = new TableProperties();
        Map searchBy = properties.getSearchBy();
        Map sortInfo = properties.getSortInfo();

    %>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">  </script>
        <script>
        function fnGotoNewTemplate(){
            document.forms['frmTemplateSearch'].action="<%= request.getContextPath()%>/template/newtemplate.view";
            document.forms['frmTemplateSearch'].submit();
        }

        function fnSort(col, order){
            var sortBy = "";
            if(order=='null' || order==null)
                sortBy = 'asc';
            else
                sortBy =order;
           $('#form-search').action='<%= request.getContextPath() %>/template/searchtemplate.view?templateName='  +  $('#templateName').val() + '&column=' + col + '&sortOrder=' + sortBy;

            $('#form-search').submit();
        }
        function fnGotoEditTemplate(templateId){
            document.forms['frmEditTemplate'].action="<%= request.getContextPath()%>/template/edittemplate.view";
            $('#id').attr('value',templateId);
            document.forms['frmEditTemplate'].submit();
        }

    $(document).ready(function(){
        $("#btnAsc").click(function(){
            /*$("#myb").attr("class", "icon-chevron-down");*/
            alert($(this).attr('ToolTip'));
        });
    });
    </script>
</head>
<body onload="fnActivateNavigation('templateListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <div class="row">
        <form class="form-search" action="<%= request.getContextPath() %>/template/searchtemplate.view" method="GET" id="frmTemplateSearch">
            <fieldset>
                <legend><spring:message code="label.template.searchTemplate"/></legend>
                    <input type="text" class="input-medium" placeholder="<spring:message code="label.template.name"/>" name="templateName" id="templateName" value="<%= searchBy.get("templateName") %>">

                    <button type="submit" class="btn"><i class="icon-search"></i></button>
                    <button type="button" class="btn" onclick="javascript:fnGotoNewTemplate()"><spring:message code="label.master.new"/></button>
            </fieldset>
        </form>
    </div>
    <div class="row">
        <c:if test="${showtable=='Y'}">
        <c:choose>
        <c:when test="${empty templates}">
            <div class="alert span6"><spring:message code="message.nodatafound"/></div>
        </c:when>
        <c:otherwise>
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th><a href="javascript:fnSort('name','<%= sortInfo.get("name") %>')"></a><spring:message code="label.template.name"/>
                            <i id="myb" class="icon-chevron-up"></i></a>

                        </th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="template" items="${templates}">
                        <tr>
                            <td><c:out value="${template.name}"/></td>
                            <td><a href="javascript:fnGotoEditTemplate('<c:out value="${template.id}"/>')"><spring:message code="label.master.edit"/></a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:otherwise>
        </c:choose>
        </c:if>
    </div>
</div>
<form id="frmEditTemplate" method="POST">
    <input id="id" name="id" type="hidden"/>
</form>
</body>
</html>