<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <script type="text/javascript">
        tinymce.init({
            menubar: false,
            selector: "textarea"
        });

        function fnValidateServer(formId){
            document.forms['frmMailServer'].action="<%= request.getContextPath()%>/mailserver/test.view";
            document.forms['frmMailServer'].method="POST";
            document.forms['frmMailServer'].submit();
        }

        function fnGotoMailServerSearch(){
            document.forms['frmMailServer'].action="<%= request.getContextPath()%>/mailserver/mailserverList.view";
            document.forms['frmMailServer'].method="GET";
            document.forms['frmMailServer'].submit();
       }
    </script>
</head>
<body onload="fnActivateNavigation('mailserverListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <div class="row">
        <form class="form-horizontal" action="<%= request.getContextPath() %>/mailserver/savemailserver.view" method="POST" id="frmMailServer">
            <fieldset>
                <legend><spring:message code="label.mailserver.mailServerDetails"/> </legend>
                <div class="control-group">
                    <label class="control-label" for="fromAddress"><spring:message code="label.mailserver.fromAddress"/></label>
                    <div class="controls">
                        <input type="text" id="fromAddress" placeholder="<spring:message code="label.mailserver.fromAddress"/>" name="fromAddress" class="input-xlarge" value="${form.bean.fromAddress}" validation="@Required @Email">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="friendlyName"><spring:message code="label.campaign.recipient.friendlyName"/></label>
                    <div class="controls">
                        <input type="text" id="friendlyName" placeholder="<spring:message code="label.campaign.recipient.friendlyName"/>" name="friendlyName" class="input-xlarge" value="${form.bean.friendlyName}" validation="@Required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="host"><spring:message code="label.mailserver.host"/></label>
                    <div class="controls">
                        <input type="text" id="host" placeholder="<spring:message code="label.mailserver.host"/>" name="host" class="input-xlarge" value="${form.bean.host}"  validation="@Required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="username"><spring:message code="label.mailserver.username"/></label>
                    <div class="controls">
                        <input type="text" id="username" placeholder="<spring:message code="label.mailserver.username"/>" name="username" class="input-xlarge" value="${form.bean.username}"  validation="@Required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password"><spring:message code="label.mailserver.password"/></label>
                    <div class="controls">
                        <input type="password" id="password" placeholder="<spring:message code="label.mailserver.password"/>" name="password" class="input-xlarge" value="${form.bean.password}" validation="@Required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="status"><spring:message code="label.mailserver.status"/>:</label>
                    <div class="controls">
                        <select id="status" name="status" class="input-medium">
                            <option value="100" <c:if test="${form.bean.status eq '100'}">selected</c:if>><spring:message code="label.mailserver.active"/></option>
                            <option value="200" <c:if test="${form.bean.status eq '200'}">selected</c:if>><spring:message code="label.mailserver.inactive"/></option>
                        </select>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" onclick="javascript:validateForm('frmMailServer')" class="btn btn-primary"><spring:message code="label.master.save"/></button>
                    <button type="button" onclick="javascript:fnValidateServer('frmMailServer')" class="btn"><spring:message code="label.master.test"/></button>
                    <button type="button" class="btn" onclick="javascript:fnGotoMailServerSearch()"><spring:message code="label.master.cancel"/></button>
                </div>
                <input type="hidden" id="id" name="id" value="${form.bean.id}"/>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>