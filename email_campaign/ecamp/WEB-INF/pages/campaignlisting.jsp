<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ page import="java.util.Map"%>
<%@ page import="com.tola.campaign.core.ObjectModel" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">


<head>
    <jsp:include page="/template/includes.jsp" />
    <script type="text/javascript">
        $(document).ready(function() {
            var html;
            var row=0;
            var per=0;
            var tabledata;

          $('#xmllink').hide();

          $('#product').change(function(){

                var product= $('#product option:selected').text();

                if(product =="Saba Enterprise"){
                    $('#sabaProduct').show();
                }else{
                    $('#sabaProduct').hide();
                }


          });




            $('#product1').change(function(){
                var id= $('#product1').val();
                $('#data').find("tr:gt(0)").remove();
                row=0;
                per=0;
                $("#filedata").val("");

                $.ajax({
                    url: '<%= request.getContextPath() %>/campaign/getObject.view?id='+id,

                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data){
                        html=data.length;

                        jQuery.each(data, function(i , item) {
                            $("#data").append('<tr id=new' +(row) + '>'+
                                    '<td  id=saba' +(row)+'>' + item.fieldName +'</td>' +
                                    '<td > ' + '<select id="candidateType'+(row)+'" name="status" class="input-medium fix-text-small">' + '</td>' +
                                    '<td >'  + '<input type="text" id=exper' +(row)+' />' + '</td>' +
                                    '<td >'  + '<input type="text" id=valu' +(row)+'>' + '</td>'                                     +'</tr>' )
                            row++;

                        });



                    }
                });

            })
            $('#transform').click(function(){

                var i=0;
                var add;
                var line="";
                while(row > i) {
                    var fieldName = $("#saba"+i).text();
                    var fieldselect = $('#candidateType'+i).val();
                   var fieldvalue= $('#candidateType'+i+' option:selected').text();

                    var expesion = $('#exper'+i).val();
                    var defau = $('#valu'+i).val();
                    var add12 = fieldName + "," + fieldselect + "," +fieldvalue +  "," + expesion + "," + defau + "|";
                    fieldName="";
                    fieldselect="";
                    expesion="";
                    defau="";
                    line+=add12;
                 /*   alert(line);*/
                    i++;
                }
                var oMyForm = new FormData();
                oMyForm.append("file", filedata.files[0]);
                var delimitor=$('#Delimitor').val();
                var product= $('#product1 option:selected').text();
                var id= $('#product1').val();
               $.ajax({
                    url: '<%= request.getContextPath() %>/campaign/transform.view?delimitor='+delimitor + '&object='+product + '&ObjectKey='+id +'&mapping='+line,
                    data: oMyForm,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        $('#xmllink').show();
                        $('#xmllink').html(data);
                    }
                });






            });

            $("#filedata").change(function(){
                var oMyForm = new FormData();
                oMyForm.append("file", filedata.files[0]);
                var id=$('#Delimitor').val();

                if(id == "" || id == null){
                    alert("Please select Delimitor and then select file ");
                    $("#filedata").val("");
                    return false
                }


                $.ajax({
                    url: '<%= request.getContextPath() %>/campaign/getCustomerFields.view?id='+id,
                    data: oMyForm,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data){
                        while(html > per) {
                            getDropDownValues(data[0], "#candidateType" +(per));
                            per++;
                        }



                    }
                });

            });


           /* $('#preview').click(function(){

                var oMyForm = new FormData();
                oMyForm.append("file", filedata.files[0]);
                var id=$('#Delimitor').val();



                $.ajax({
                    url: '<%= request.getContextPath() %>/campaign/getCustomerFields.view?id='+id,
                    data: oMyForm,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function(data){
                       while(html > per) {
                           getDropDownValues(data[0], "#candidateType" +(per));
                           per++;
                       }



                    }
                });



            });*/

            function getDropDownValues(list, typeId){
                var j=0;

                var html = '<option value="">Select</option>';

                jQuery.each(list, function(i , item) {
                    html += '<option value="'+j+'">'+item+'</option>';
                    j++;
                });
                jQuery(typeId).append(html);


            }



        });
    </script>

    <%--<%
        TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
        if(properties==null)

            properties = new TableProperties();

        Map searchBy = properties.getSearchBy();
        Map sortInfo = properties.getSortInfo();
        int rows = properties.getTotalrows();
        int col=properties.getCurrentPage();
        int begin=properties.getBegin();
        int end=properties.getEnd();
        int pgs=properties.getTotalPages();

    %>

        function fnSort(col, order){
            var sortBy = "";
            if(order=='null' || order==null)
                sortBy = 'ASC';
            else
                sortBy =order;
            window.location.assign("<%= request.getContextPath() %>/campaign/searchcampaign.view?campaignName="+ escape($('#campaignName').val()) + "&column=" + col + "&sortOrder=" + sortBy);
        }
        function fnGotoEditCampaign(campaignid){
            document.forms['frmEditCampaign'].method='GET';
            document.forms['frmEditCampaign'].action="<%= request.getContextPath()%>/campaign/editcampaign.view?id=" + campaignid;
            document.forms['frmEditCampaign'].submit();
        }

    </script>--%>

    <script>
        function submit(){
            alert("john");

        }
    </script>
</head>
<body onload="fnActivateNavigation('campaignListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <div class="row">
        <form class="form-search" action="<%= request.getContextPath() %>/campaigns/getCustomerFields.view" id="frmCampaignRecipient" enctype="multipart/form-data" method="POST" >
            <fieldset>
                <legend>Design Product</legend>

                <label class="control-label" >Saba Product:</label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select id="product" name="sabaProduct" class="input-medium">
                    <c:forEach items="${object1}" var="keyword">
                        <option value=<c:out value="${keyword.id}"/> ><c:out value="${keyword.name}"/></option>
                    </c:forEach>
                </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="sabaProduct" placeholder="Saba Product" name="sabaProduct1" class="input-medium" value="">


            </fieldset>
            <br/>
            <fieldset>


                <label class="control-label" >Objects:</label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select id="product1" name="objectProduct" class="input-medium">
                    <c:forEach items="${object}" var="keyword">
                        <option value=<c:out value="${keyword.id}"/> ><c:out value="${keyword.name}"/></option>
                    </c:forEach>
                </select>&nbsp;&nbsp;

                <a href="#top" id="xmllink">sadasdas</a>


            </fieldset>
            <br/>

            <fieldset>


                <label class="control-label" >Delimitor:</label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" id="Delimitor" placeholder="Delimitor" name="delimitor" class="input-medium" >

            </fieldset><br/>

            <fieldset>


                <label class="control-label" >Upload File:</label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="file" name="filedata"  validation="@Required" id="filedata"/>
                &nbsp;&nbsp;&nbsp;
                <%--<button type="button" class="btn btn-primary" id="preview" >Get Fields</button>--%>
                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" id="save" >Save mapping</button>
                &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" id="transform" >Transform</button>



            </fieldset>
            <br/>
            <br/>
            <table id = "data" class="table table-nonfluid table-bordered table-striped">
                <thead>
                <tr>

                    <th>Saba Fields</th>
                    <th>Custeomer Fields</th>

                    <th>Default Value</th>

                    <th>Expression</th>

                </tr>
                </thead>
                <%--<tr>
                    <td>Name</td>
                    <td><select id="candidateType" name="status" class="input-medium">

                    </select></td>
                    <td> <input type="text" /></td>
                    <td> <input type="text" /></td>


                </tr>
                <tr>
                    <td>Id</td>
                    <td><select id="candidateType2" name="status" class="input-medium fix-text-small">

                    </select></td>
                    <td> <input type="text" /></td>
                    <td> <input type="text" /></td>


                </tr>
                <tr>
                    <td>Domain</td>
                    <td><select id="candidateType3" name="status" class="input-medium fix-text-small">

                    </select></td>
                    <td> <input type="text" /></td>
                    <td> <input type="text" /></td>


                </tr>
                <tr>
                    <td>Description</td>
                    <td><select id="candidateType4" name="status" class="input-medium fix-text-small">


                    </select></td>
                    <td> <input type="text" /></td>
                    <td> <input type="text" /></td>

                </tr>
--%>
            </table>



        </form>
    </div>
    <div class="row">
        <%--<c:if test="${showtable=='Y'}">
            <c:choose>
            <c:when test="${empty campaigns}">
                <div class="alert span6"><spring:message code="message.nodatafound"/></div>
            </c:when>
            <c:otherwise>--%>
        <%-- <table class="table table-striped table-condensed table-bordered">
             <thead>
                 <tr>
                     <th><a href="javascript:fnSort('NAME','<%= sortInfo.get("NAME") %>')">
                         <spring:message code="label.campaign.name"/>
                         <% if(properties.getSortClass("NAME")!=null){%>
                         <i id="myb" class="<%= properties.getSortClass("NAME")%>"></i>
                         <% } %>
                     </a>

                     </th>
                     <th><a href="javascript:fnSort('STATUS','<%= sortInfo.get("STATUS") %>')">
                         <spring:message code="label.campaign.status"/>
                                     <% if(properties.getSortClass("STATUS")!=null){%>
                             <i id="myb" class="<%= properties.getSortClass("STATUS")%>"></i>
                                     <% }  %>
                     </th>
                     <th>&nbsp;</th>
                 </tr>
             </thead>
             <tbody>
                &lt;%&ndash; <c:forEach var="campaign" items="${campaigns}" begin="<%=begin%>" end="<%=end%>">
                     <tr>
                         <td><c:out value="${campaign.name}"/></td>
                         <td><c:choose>
                                 <c:when test="${campaign.status=='100'}">
                                     <spring:message code="label.campaign.active"/>
                                 </c:when>
                                 <c:when test="${campaign.status=='200'}">
                                     <spring:message code="label.campaign.inactive"/>
                                 </c:when>
                                 <c:when test="${campaign.status=='300'}">
                                     <spring:message code="label.campaign.inProgress"/>
                                 </c:when>
                                 <c:when test="${campaign.status=='400'}">
                                     <spring:message code="label.campaign.complete"/>
                                 </c:when>
                                 <c:when test="${campaign.status=='500'}">
                                     <spring:message code="label.campaign.paused"/>
                                 </c:when>
                                 <c:when test="${campaign.status=='600'}">
                                     <spring:message code="label.campaign.abort"/>
                                 </c:when>
                         </c:choose>
                         <td><a href="<%= request.getContextPath()%>/campaign/editcampaign.view?id=<c:out value='${campaign.id}'/>"><spring:message code="label.master.edit"/></a>&nbsp;&nbsp;&nbsp;
                             <c:if test="${campaign.status=='400'}">
                                 <a href="<%= request.getContextPath()%>/campaign/detailscampaign.view?id=<c:out value='${campaign.id}'/>"><spring:message code="label.campaign.details"/></a>
                             </c:if>
                         </td>

                     </tr>
                 </c:forEach>
                &ndash;%&gt; <tr>
                     <td colspan="3">
                         <jsp:include page="/template/pagination.jsp"></jsp:include>
                     </td>
                 </tr>
             </tbody>
         </table>
     </c:otherwise>
     </c:choose>
 </c:if>
</div>
</div>--%>
</body>
</html>