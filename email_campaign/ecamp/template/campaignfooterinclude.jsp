<script>

    $('#recipientList').click(function (e) {
        e.preventDefault();

    });

    $('#home').click(function (e) {
        e.preventDefault();
        $('#homeLi').addClass('active');
        $('#templateLi').removeClass('active');
        $('#recipientLi').removeClass('active');
        $('#scheduleLi').removeClass('active');

        $('#homeTab').addClass("active");
        $('#recipientListTab').removeClass("active");
        $('#templateTab').removeClass("active");
        $('#scheduleTab').removeClass("active");
    });

    $('#templateList').click(function (e) {
        e.preventDefault();
        $('#homeLi').removeClass('active');
        $('#templateLi').addClass('active');
        $('#recipientLi').removeClass('active');
        $('#scheduleLi').removeClass('active');

        $('#homeTab').removeClass("active");
        $('#recipientListTab').removeClass("active");
        $('#templateTab').addClass("active");
        $('#scheduleTab').removeClass("active");
    });

    $('#scheduleList').click(function (e) {
        e.preventDefault();
        $('#homeLi').removeClass('active');
        $('#templateLi').removeClass('active');
        $('#recipientLi').removeClass('active');
        $('#scheduleLi').addClass('active');

        $('#homeTab').removeClass("active");
        $('#recipientListTab').removeClass("active");
        $('#templateTab').removeClass("active");
        $('#scheduleTab').addClass("active");
    });


    tinymce.init({
        menubar: false,
        selector: "textarea"
    });

</script>