<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <script type="text/javascript">
        tinymce.init({
            menubar: false,
            selector: "textarea"
        });

        function fnAddToBody(){
            var keywordSelect = document.getElementById('keyword');
            var keyword = keywordSelect.options[keywordSelect.selectedIndex].text;
            var editor = tinyMCE.get('templatebody');
            var content = editor.getContent();
            editor.execCommand('mceInsertContent',false,' {' +  keyword + '} ');
        }

        function fnAddToSubject(){
            var keywordSelect = document.getElementById('keyword');
            var keyword = keywordSelect.options[keywordSelect.selectedIndex].text;
            document.getElementById('subject').value += '{' + keyword + '}';
        }

        function testTemplate(templateId,campaignId){
            window.open('<%= request.getContextPath()%>/campaign/testmail.view?templateId=' + templateId + '&campaignId=' + campaignId,
                        "location=no,menubar=no,status=no,resizable=no");
        }

        function fnSelectExisting(campaignId){
            window.open('<%= request.getContextPath()%>/template/defaultbasesearch.view?campaignId=' + campaignId,
                    "location=no,menubar=no,status=no,resizable=no");
        }
    </script>
</head>
<body onload="fnActivateNavigation('campaignListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <jsp:include page="/template/campaigninclude.jsp"/>
        <c:if test="${not empty form.bean.templates}">
            <div class="row">
                <table class="table table-condensed table-striped table-bordered span6">
                    <thead>
                        <tr>
                            <th><spring:message code="label.template.name"/></th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${form.bean.templates}" var="template">
                            <tr>
                                <td><c:out value="${template.name}"/></td>
                                <td><a href="<%= request.getContextPath()%>/campaign/editcampaigntemplate.view?id=<c:out value='${template.id}'/>&campaignId=<c:out value='${template.ownerId}'/>"><spring:message code="label.master.edit"/></a>&nbsp;&nbsp;&nbsp;
                                    <a href="<%= request.getContextPath()%>/campaign/deletecampaigntemplate.view?id=<c:out value='${template.id}'/>&campaignId=<c:out value='${template.ownerId}'/>"><spring:message code="label.master.delete"/></a>&nbsp;&nbsp;&nbsp;
                                    <a href="javascript:testTemplate('<c:out value='${template.id}'/>','<c:out value='${template.ownerId}'/>');"><spring:message code="label.master.testMail"/></a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:if>
        <form class="form-horizontal"  method="POST" id="frmTemplate" action="<%= request.getContextPath()%>/campaign/campaigntemplatesave.view">
            <fieldset>
                <legend><spring:message code="label.template.templateDetails"/> </legend>
                <div class="control-group">
                    <label class="control-label" for="templateName"><spring:message code="label.template.name"/></label>
                    <div class="controls">
                        <input type="text" placeholder="<spring:message code="label.template.name"/>" name="templateEntity.name"
                               class="input-xlarge" value="${form.bean.templateEntity.name}" id="templateName"
                               validation="@Required" maxlength="50">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="subject"><spring:message code="label.template.subject"/></label>
                    <div class="controls">
                        <input type="text" id="subject" placeholder="<spring:message code="label.template.subject"/>" name="templateEntity.subject"
                               class="input-xlarge" value="${form.bean.templateEntity.subject}"
                               validation="@Required" maxlength="255">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="keyword"><spring:message code="label.template.keyword"/></label>
                    <div class="controls">
                        <select id="keyword">
                            <c:forEach items="${form.bean.keywords}" var="keyword">
                                <option><c:out value="${keyword}"/></option>
                            </c:forEach>
                        </select>
                        <button type="button"  onclick="javascript:fnAddToBody()" class="btn"><spring:message code="label.template.addToBody"/></button>
                        <button type="button"  onclick="javascript:fnAddToSubject()" class="btn"><spring:message code="label.template.addToSubject"/></button>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="templatebody" ><spring:message code="label.template.body"/></label>
                    <div class="controls">
                        <textarea id="templatebody"
                                  placeholder="<spring:message code="label.template.body"/>"
                                  name="templateEntity.body" class="input-medium"
                                  >${form.bean.templateEntity.body}</textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="status"><spring:message code="label.template.status"/>:</label>
                    <div class="controls">
                        <select id="status" name="templateEntity.status" class="input-medium">
                            <c:forEach items="${statuses}" var="option">
                                <option value="<c:out value='${option.id}'/>"
                                        <c:if test="${form.bean.templateEntity.status eq option.id}">selected</c:if>>
                                    <spring:message code="${option.text}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button"  onclick="javascript:validateForm('frmTemplate')" class="btn btn-primary"><spring:message code="label.master.save"/></button>
                        <a  class="btn"
                            href="javascript:fnSelectExisting('<c:out value="${form.bean.ownerId}"/>')" ><spring:message code="label.campaign.addExisting"/></a>
                    <a  href="<%= request.getContextPath()%>/campaign/campaigntemplate.view?id=<c:out value="${form.bean.ownerId}"/>" class="btn"><spring:message code="label.master.addAnother"/></a>
                    <c:if test="${form.bean.attachedSchedule}">
                        <a  class="btn"
                            href="<%= request.getContextPath()%>/campaign/campaignschedule.view?id=<c:out value="${form.bean.ownerId}"/>" ><spring:message code="label.campaign.addSchedule"/></a>
                    </c:if>
                    <button type="button" class="btn" onclick="javascript:fnGotoCampaignSearch('frmTemplate')"><spring:message code="label.master.cancel"/></button>
                </div>
                <input type="hidden" name="templateEntity.id" value="${form.bean.templateEntity.id}"/>
                <input type="hidden" name="ownerId" value="${form.bean.ownerId}"/>
            </fieldset>
        </form>
</div>
</body>
</html>