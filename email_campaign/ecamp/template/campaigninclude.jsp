<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ page import="com.tola.dto.CampaignDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<script>
    function fnGotoCampaignSearch(formId){
        document.forms[formId].action="<%= request.getContextPath()%>/campaign/campaignList.view";
        document.forms[formId].method="GET";
        document.forms[formId].submit();
    }
</script>
<ul class="breadcrumb">
    <%
        Map<String, Breadcrumb> breadcrumbs = BreadcrumbUtil.getBreadCrumbsForCampaign();
        FormBean formBean = ((FormBean)request.getAttribute("form"));
        CampaignDTO dto = (CampaignDTO)formBean.getBean();
        Breadcrumb crumb = breadcrumbs.get("home");
        if(dto.getGotToTab().equals("home")){
    %>
                <li id='<%= crumb.getLinkId() %>' class="active">
                    <%= crumb.getLabel() %> /
                </li>
    <%
        }else{
    %>
                <li id='<%= crumb.getLinkId() %>'>
                    <a href="<%= crumb.getGotoPage() %>?id=<%= ((CampaignDTO)formBean.getBean()).getOwnerId() %>" id='<%= crumb.getListId() %>'><%= crumb.getLabel() %></a> /
                </li>
    <%
        }
        if(dto.isAttachedRecipients()){
            crumb = breadcrumbs.get("recipient");
            if(dto.getGotToTab().equals("recipient")){
    %>
                <li id='<%= crumb.getLinkId() %>' class="active">
                    <%= crumb.getLabel() %> /
                </li>
    <%
            }else{
    %>
                <li id='<%= crumb.getLinkId() %>'>
                    <a href="<%= crumb.getGotoPage() %>?id=<%= ((CampaignDTO)formBean.getBean()).getOwnerId() %>" id='<%= crumb.getListId() %>'><%= crumb.getLabel() %></a> /
                </li>
    <%
            }
        }
        if(dto.isAttachedTemplates()){
            crumb = breadcrumbs.get("template");
            if(dto.getGotToTab().equals("template")){
    %>
                <li id='<%= crumb.getLinkId() %>' class="active">
                    <%= crumb.getLabel() %> /
                </li>
    <%
            }else{
    %>
                <li id='<%= crumb.getLinkId() %>'>
                    <a href="<%= crumb.getGotoPage() %>?id=<%= ((CampaignDTO)formBean.getBean()).getOwnerId() %>" id='<%= crumb.getListId() %>'><%= crumb.getLabel() %></a> /
                </li>
    <%
            }
        }
        if(dto.isAttachedSchedule()){
            crumb = breadcrumbs.get("schedule");
            if(dto.getGotToTab().equals("schedule")){
    %>
                <li id='<%= crumb.getLinkId() %>' class="active">
                    <%= crumb.getLabel() %> /
                </li>
    <%
            }else{
    %>
                <li id='<%= crumb.getLinkId() %>'>
                    <a href="<%= crumb.getGotoPage() %>?id=<%= ((CampaignDTO)formBean.getBean()).getOwnerId() %>" id='<%= crumb.getListId() %>'><%= crumb.getLabel() %></a> /
                </li>
    <%
            }
        }
    %>
</ul>
