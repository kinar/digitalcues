<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <script type="text/javascript">
        function fnValidateEmails(errors){
            var checked =$('input[name=emailProperty]:checked').length;
            if(checked==0){
                errors.push(getErrorMessage('<spring:message code="message.error.emailCheckbox"/>',null));
            }
        }
        function fnSaveAndActivateSchedule(){
            document.forms['frmSchedule'].elements['campaign.status'].value='100';
            $('#emailPropertiesTable').attr('validation','@Custom(fn=fnValidateEmails)');
            validateForm('frmSchedule');
        }

        function fnSaveSchedule(){
            $('#emailPropertiesTable').removeAttr('validation');
            validateForm('frmSchedule');
        }
    </script>
</head>
<body onload="fnActivateNavigation('campaignListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <jsp:include page="/template/campaigninclude.jsp"/>
        <form class="form-horizontal"  method="POST" id="frmSchedule" action="<%= request.getContextPath()%>/campaign/campaignschedulesave.view">
            <fieldset>
                <legend><spring:message code="label.campaign.scheduleDetails"/> </legend>
                <div class="control-group">
                    <label class="control-label" for="scheduleStartDate"><spring:message code="label.schedule.startDate"/></label>
                    <div class="controls">
                        <c:set var="startDate" value="${form.bean.schedule.startDate}"/>

                        <input type="text" placeholder="<spring:message code="label.schedule.startDate"/>" name="schedule.startDate"
                               class="input-small datepicker" value="<f:formatDate value='${startDate}' pattern="MM/dd/yyyy"/>"
                               validation="@Date(Range=future)" readonly label="<spring:message code="label.schedule.startDate"/>" id="scheduleStartDate">
                        <input type="text" placeholder="<spring:message code="label.schedule.startTime"/>" name="schedule.startTime" class="input-small"
                               value="${form.bean.schedule.startTime}" validation="@Time" label="<spring:message code="label.schedule.startTime"/>"
                               id="scheduleStartTime">
                        <span><spring:message code="label.schedule.every"/></span>
                        <input type="text" placeholder="<spring:message code="label.schedule.interval"/>" name="schedule.timeFrequency" class="input-small" value="${form.bean.schedule.timeFrequency}" validation="@Integer(Range=1-100)"
                               label="<spring:message code="label.schedule.interval"/>" >
                        <select name="schedule.timeFrequencyUnit" class="input-small">
                            <option value="M">Minutes</option>
                            <option value="H">Hours</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="batchSize"><spring:message code="label.schedule.batchSize"/></label>
                    <div class="controls">
                        <input type="text" placeholder="<spring:message code="label.schedule.batchSize"/>" name="campaign.batchSize" class="input-mini" value="${form.bean.campaign.batchSize}" validation="@Integer(Range=1-10000)" id="batchSize" >
                        <input type="hidden" value="${form.bean.campaign.status}" name="campaign.status" id="campaignStatus">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><spring:message code="label.schedule.emailProperty"/></label>

                    <table id="emailPropertiesTable" class="table table-condensed table-striped row span8" >
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th><spring:message code="label.schedule.fromAddress"/></th>
                                <th><spring:message code="label.schedule.host"/></th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${form.bean.propertiesMap}" var="entry">
                            <tr>
                                <td><input type="checkbox" value="<c:out value='${entry.key.id}'/>" name="emailProperty" <c:if test="${entry.value =='Y'}">checked</c:if> class="emailCheckbox"/></td>
                                <td><c:out value="${entry.key.fromAddress}"/></td>
                                <td><c:out value="${entry.key.host}"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="form-actions">
                    <button type="button"  onclick="javascript:fnSaveSchedule()" class="btn btn-primary"><spring:message code="label.master.save"/></button>
                    <button type="button"  onclick="javascript:fnSaveAndActivateSchedule()" class="btn"><spring:message code="label.master.saveAndActivate"/></button>
                    <button type="button" class="btn" onclick="javascript:fnGotoCampaignSearch('frmSchedule')"><spring:message code="label.master.cancel"/></button>
                </div>
                <input type="hidden" name="schedule.id" value="${form.bean.schedule.id}"/>
                <input type="hidden" name="ownerId" value="${form.bean.ownerId}"/>
            </fieldset>
        </form>
</div>
</body>
</html>