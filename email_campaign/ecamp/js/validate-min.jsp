<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

var requiredMessage ='<spring:message code="message.error.required"/>';
var lengthMessage ='<spring:message code="message.error.length"/>';
var emailFormat = '<spring:message code="message.error.emailFormat"/>';
var dateFormat = '<spring:message code="message.error.dateFormat"/>';
var timeFormat ='<spring:message code="message.error.timeFormat"/>';
var reconfirmMatcher ='<spring:message code="message.error.confirmMatcher"/>';
var intformat ='<spring:message code="message.error.intFormat"/>';
var optionalRequiredMessage ='<spring:message code="message.error.optionalRequired"/>';
var pastDateMessage = '<spring:message code="message.error.pastDateMessage"/>';
var futureDateMessage = '<spring:message code="message.error.futureDateMessage"/>';
var intRangeFormat = '<spring:message code="message.error.intRangeFormat"/>';
function validateForm(formId){
    if($('#validationError').length>0)
        $('#validationError').remove();
    var elements = new Array();
    $('#' + formId).find("input[type=text],input[type=file],input[type=password],textarea,select, table").each(function(){
       elements.push($(this));
    });
    var errors = new Array();
    for(var i=0;i<elements.length;i++){
        var validation= $(elements[i]).attr("validation");
        var labelName = getLabelForElement(elements[i]);
        if(validation!=undefined){
            var validations = validation.split(' ');
            for(var j=0;j<validations.length;j++)
            {
               if(validations[j].trim()=='@Required')
                validateRequired(elements[i],errors, labelName);
               if(validations[j].indexOf('@Length')>=0)
                validateLength(elements[i],errors, labelName, validations[j].trim());
               if(validations[j].indexOf('@Matcher')>=0)
                validateMatcher(elements[i],errors, labelName, validations[j].trim());
               if(validations[j].trim()=='@Email')
                validateEmail(elements[i],errors, labelName);
               if(validations[j].trim().indexOf('@Date')>=0)
                validateDate(elements[i],errors, labelName,validations[j]);
               if(validations[j].trim()=='@Time')
                validateTime(elements[i],errors, labelName);
               if(validations[j].trim().indexOf('@Integer')>=0)
                validateInteger(elements[i],errors, labelName,validations[j]);
               if(validations[j].trim().indexOf('@OptionalRequired')>=0)
                validateOptional(elements[i],errors,validation)
               if(validations[j].trim().indexOf('@Custom')>=0)
                validateCustom(validations[j],errors);
            }
        }
    }

    if(errors.length!=0){
        showErrors(errors, formId);
    }else{
        var frm = document.forms[formId];
        frm.submit();
        return;
    }

}

function validateCustom(validation,errors){
    var functionName = validation.substring('@Custom(fn='.length,validation.length-1);
    window[functionName](errors);
}

function validateOptional(element,errors,validation){
    var fieldNames = validation.substring('@OptionalRequired(fields='.length, validation.length-1).trim();
    var fields = fieldNames.split(",");
    for(var i=0;i< fields.length;i++){
        var value = $('#' + fields[i]).val().trim();
        if(value!='') return;
    }
    var param = '';
    for(var i=0;i< fields.length;i++){
        console.log(fields[i]);
        param = param + getLabelForElement($('#' + fields[i])) + ',';
    }
    param = param.substring(0,param.length-1);
    errors.push(getErrorMessage(optionalRequiredMessage, [param]));
}

function showErrors(errors, formId){
    var message = "";
    for(var i=0;i<errors.length;i++)
    message = message + errors[i] + "<br/>";
    $('<div/>', {
    id: 'validationError',
    class: 'alert alert-error',
    html: message
    }).prependTo('#' + formId);
}

function validateRequired(element, errors, labelName){
    var value = $(element).val();
    if(isNullOrEmpty(value))
        errors.push(getErrorMessage(requiredMessage,[labelName]));
}

function validateDate(element, errors, labelName,validation){
    var value = $(element).val();
    if(value!=null && value.length!=10){
        errors.push(getErrorMessage(dateFormat,[labelName]));
        return;
    }
    var maxDays = [0,31,28,31,30,31,30,31,31,30,31,30,31];
    var splits = value.split('/');
    if(splits.length!=3){
        errors.push(getErrorMessage(dateFormat,[labelName]));
        return;
    }
    var day = parseInt(splits[1]);
    var mth = parseInt(splits[0]);
    var year = parseInt(splits[2]);
    if(mth==2){
        var lyear = false;
        if((!(year % 4) && year % 100) || !(year % 400)){
            lyear = true;
        }
        if((lyear && day>29) || (!lyear && day>28)){
            errors.push(getErrorMessage(dateFormat,[labelName]));
            return;
        }
    }else{
        if(day<=0 && day>maxDays[mth]){
            errors.push(getErrorMessage(dateFormat,[labelName]));
            return;
        }
    }

    if(validation!='@Date'){
        var range = validation.substring('@Date(Range='.length,validation.length-1);
        var enteredDate = new Date(splits[2],splits[0]-1,splits[1]);
console.log(enteredDate.getTime());
        var tDate=  new Date();
        var today = new Date(tDate.getFullYear(), tDate.getMonth(), tDate.getDate());

        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
console.log(today.getTime());
        if(range=='future'){
            if(enteredDate.getTime()< today.getTime()){
                errors.push(getErrorMessage(futureDateMessage,[labelName]));
            }
        }else if(range=='past'){
            if(enteredDate.getTime()> today.getTime()){
                errors.push(getErrorMessage(pastDateMessage,[labelName]));
            }
        }
    }
}

function validateInteger(element, errors, labelName,validation){
    var i;
    var s = $(element).val().toString();
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (isNaN(c))
        {
            errors.push(getErrorMessage(intformat,[labelName]));
            return;
        }
    }
    if(validation!='@Integer'){
        var range = validation.substring('@Integer(Range='.length,validation.length-1);
        var splits = range.split('-');
        var lowerBound = parseInt(splits[0]);
        var upperBound = parseInt(splits[1]);
        if(parseInt(s)< lowerBound || parseInt(s)>upperBound){
            errors.push(getErrorMessage(intRangeFormat,[labelName, lowerBound, upperBound]));
            return;
        }
    }
}

function validateLength(element, errors, labelName, validation){
    var value = $(element).val();
    var last = validation.length-1;
    var len = validation.substring("@Length(".length, last);
    var lengths = len.split(",");
    var minLength = eval(lengths[0].substring("min=".length));
    var maxLength = eval(lengths[1].substring("max=".length));
    if(value!=null && (value.length < minLength || value.length > maxLength))
        errors.push(getErrorMessage(lengthMessage,[labelName, minLength, maxLength]));
}

function validateMatcher(element, errors, labelName, validation){
    var matchElementId = validation.substring("@Matcher(id=".length , validation.length-1);
    var oriValue = $("#" + matchElementId).val();
    var value = $(element).val();
    if(oriValue!=value){
        var oriLabel = getLabelForElement($('#' + matchElementId));
        errors.push(getErrorMessage(reconfirmMatcher,[oriLabel, labelName]));
    }
}

function validateEmail(element, errors, labelName){
    var re = /\S+@\S+\.\S+/;
    if(!re.test($(element).val())){
        errors.push(getErrorMessage(emailFormat,[labelName]));
    }
}

function validateTime(element, errors, labelName){
    var value = $(element).val();
    if(value==null || value.trim()=='')
        return;
    if(value.length!=5)
    {
        errors.push(getErrorMessage(timeFormat,[labelName]));
        return;
    }
    var splits = value.split(':');
    if(splits.length!=2){
        errors.push(getErrorMessage(timeFormat,[labelName]));
        return;
    }
    if(splits[0].length!=2 || splits[0]<0 || splits[0]>23){
        errors.push(getErrorMessage(timeFormat,[labelName]));
        return;
    }
    if(splits[1].length!=2 || splits[1]<0 || splits[0]>59){
        errors.push(getErrorMessage(timeFormat,[labelName]));
        return;
    }

}

function isNullOrEmpty(value){
    return (value == null || value.trim()=='');
}

function getLabelForElement(element){
    if ($(element).is('[label]')) {
        return $(element).attr("label");
    }
    var nameOfLabel =$(element).attr("id");
    return $("label[for='" + nameOfLabel + "']").html();
}

function getErrorMessage(message, params){
    var html = message;
    if(params==null)
        return html;
    for(var i=0;i<params.length;i++){
        html=html.split("{" + i + "}").join(params[i]);
    }
    return html;
}



