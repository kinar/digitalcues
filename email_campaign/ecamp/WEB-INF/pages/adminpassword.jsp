<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />

</head>
<body onload="fnActivateNavigation('changePasswordTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <form class="form-horizontal" action="<%= request.getContextPath() %>/admin/changepassword.view" method="POST"
          id="frmPassword">
        <fieldset>
            <legend><spring:message code="label.user.changePassword"/> </legend>
            <div class="control-group">
                <label class="control-label" for="password"><spring:message code="label.user.password"/></label>
                <div class="controls">
                    <input type="password" id="password" placeholder="<spring:message code="label.user.password"/>" name="password" class="input-medium">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="repassword"><spring:message code="label.user.repassword"/></label>
                <div class="controls">
                    <input type="password" id="repassword" placeholder="<spring:message code="label.user.password"/>"class="input-medium">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary"><spring:message code="label.master.save"/></button>
                <button type="button" class="btn"><spring:message code="label.master.cancel"/></button>
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>