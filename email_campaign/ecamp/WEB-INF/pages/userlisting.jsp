<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ page import="java.util.Map"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="/template/includes.jsp" />
    <%
        TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
        if(properties==null)

            properties = new TableProperties();

        Map searchBy = properties.getSearchBy();
        Map sortInfo = properties.getSortInfo();
        int rows = properties.getTotalrows();
        int col=properties.getCurrentPage();
        int begin=properties.getBegin();
        int end=properties.getEnd();
        int pgs=properties.getTotalPages();

    %>
    <script>
        function fnGotoNewUser(){
            document.forms['frmUserSearch'].action="<%= request.getContextPath()%>/admin/newuser.view";
            document.forms['frmUserSearch'].submit();
        }

        function fnSort(col, order){
            var sortBy = "";
            if(order=='null' || order==null)
                sortBy = 'ASC';
            else
                sortBy =order;

            window.location.assign("<%= request.getContextPath() %>/admin/searchuser.view?username="+escape($('#username').val()) + "&column=" + col + "&sortOrder=" + sortBy);
        }
        function fnGotoEditUser(userid){
            document.forms['frmEditUser'].action="<%= request.getContextPath()%>/user/edituser.view";
            $('#id').attr('value',userid);
            document.forms['frmEditUser'].submit();
        }
    </script>
</head>
<body onload="fnActivateNavigation('userListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <div class="row">
        <form class="form-search" action="<%= request.getContextPath() %>/admin/searchuser.view" method="GET" id="frmUserSearch">
            <fieldset>
                <legend><spring:message code="label.user.searchUser"/></legend>

                    <input type="text" class="input-medium" placeholder="<spring:message code="label.login.username"/>" name="username" id="username" validation="@Required" label="<spring:message code="label.login.username"/>"
                    value="<%= searchBy.get("username")==null?"":searchBy.get("username") %>">

                    <button type="button" class="btn" onclick="javascript:validateForm('frmUserSearch')"><i class="icon-search"></i></button>
                    <button type="button" class="btn" onclick="javascript:fnGotoNewUser()"><spring:message code="label.master.new"/></button>
            </fieldset>
        </form>
    </div>
    <div class="row">
        <c:if test="${showtable=='Y'}">
            <c:choose>
                <c:when test="${empty users}">
                    <div class="alert span6"><spring:message code="message.nodatafound"/></div>
                </c:when>
                <c:otherwise>
                    <table class="table table-striped table-condensed table-bordered">
                        <caption><spring:message code="label.user.users"/></caption>
                        <thead>
                            <tr>
                                <th><a href="javascript:fnSort('FIRST_NAME','<%= sortInfo.get("FIRST_NAME") %>')">
                                    <spring:message code="label.user.firstName"/>
                                    <% if(properties.getSortClass("FIRST_NAME")!=null){%>
                                    <i id="myb" class="<%= properties.getSortClass("FIRST_NAME")%>"></i>
                                    <% }  %>
                                </a>
                                </th>
                                <th><a href="javascript:fnSort('LAST_NAME','<%= sortInfo.get("LAST_NAME") %>')">
                                    <spring:message code="label.user.lastName"/>
                                                <% if(properties.getSortClass("LAST_NAME")!=null){%>
                                        <i id="myb" class="<%= properties.getSortClass("LAST_NAME")%>"></i>
                                                <% }  %>
                                </th>
                                <th><spring:message code="label.login.username"/></th>
                                <th><spring:message code="label.user.email"/></th>
                                <th><spring:message code="label.user.userStatus"/></th>
                                <th>Role</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="user" items="${users}">
                                <tr>
                                    <td><c:out value="${user.firstName}"/></td>
                                    <td><c:out value="${user.lastName}"/></td>
                                    <td><c:out value="${user.username}"/></td>
                                    <td><c:out value="${user.email}"/></td>
                                    <td><c:choose><c:when test="${user.status eq '100'}">
                                                    <spring:message code="label.user.active"/>
                                                  </c:when>
                                                  <c:otherwise>
                                                    <spring:message code="label.user.inactive"/>
                                                  </c:otherwise>
                                    </c:choose></td>
                                    <td>
                                    <c:choose><c:when test="${user.role eq '100'}">
                                        <spring:message code="label.user.user"></spring:message>
                                    </c:when>
                                        <c:otherwise>
                                            <spring:message code="label.user.superuser"></spring:message>
                                        </c:otherwise>
                                    </c:choose>
                                    </td>
                                    <td><a href="javascript:fnGotoEditUser('<c:out value="${user.id}"/>')"><spring:message code="label.master.edit"/></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>
        </c:if>
    </div>
</div>
<form id="frmEditUser" method="GET">
    <input id="id" name="id" type="hidden"/>
</form>
</body>
</html>