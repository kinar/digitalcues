<%@ page import="java.util.List" %>
<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <jsp:include page="/template/includes.jsp" />
</head>
<body>
<c:if test="${not empty form.feedback}">
    <span class="alert alert-success span10" style="z-index: 999">
            <spring:message code="${form.feedback}"/>
    </span>
</c:if>
<c:if test="${not empty form.exception}">
    <span class="alert alert-error span10">
            ${form.exception.message}
    </span>
</c:if>

</body>
</html>