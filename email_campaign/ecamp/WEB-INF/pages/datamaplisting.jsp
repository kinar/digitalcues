<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ page import="java.util.Map"%>
<%@ page import="com.tola.campaign.core.ObjectModel" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">


<head>
<jsp:include page="/template/includes.jsp" />
<script type="text/javascript">
    $(document).ready(function() {
        var html;
        var row=0;
        var per=0;
        var tabledata;
        var csvFieldListRestore = "";
        reset();
        var validateArray=[];

        function reset()
        {
            $('#delimitor').val("");
            $("#savedMappings").html("");
            $("#filedata").val("");
            $('#data').find("tr:gt(0)").remove();
            $('#xmllink').hide();
            $('#csvlink').hide();
            $("#savedMappings").hide();
            csvFieldListRestore ="";
        }

        function loadMappingValues(list)
        {
            jQuery.each(list, function(i , item) {
                var key = item.key;
                $("#candidateType" + key).val(item.csvField);
                $("#valu" +key).val(item.default_val);
            });
        }

        function loadSavedMappings(data)
        {
           $('#delimitor').val(data.delimitor);
           per =0;
           html = data.csvFieldList.length;
           while(html > per)
           {
               $("#candidateType" +(per)).html("");
               csvFieldListRestore = data.csvFieldList;
               getDropDownValues(data.csvFieldList, "#candidateType" +(per));
               per++;
           }
           loadMappingValues(data.fieldList);

        }

        $('#product').change(function(){
            reset();
            $("#object").val("-1");
            $('#object').empty();
            $("#object").append('<option value="-1">Object Name</option>');
            var product= $('#product option:selected').text();
            var sabaVersion = $("#sabaProduct").val();
            if(product =="Saba Enterprise"){
                $('#sabaProduct').show();
                $('#version').show();
                $.ajax({
                            url: '<%= request.getContextPath() %>/objects/getSEObject.view?&sabaproduct='+ product + '&sabaVersion='+ sabaVersion,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function(data){
                            alert(data);
                            jQuery.each(data, function(i , item) {
                                $("#object").append('<option value="' + item.id + '">' + item.name + '</option>');
                            });
                        }
                     });
            }else{
                $('#sabaProduct').hide();
                $('#version').hide();
                $.ajax({
                           url: '<%= request.getContextPath() %>/objects/getSCObject.view?&sabaproduct='+ product,
                           processData: false,
                           contentType: false,
                           type: 'POST',
                           success: function(data){
                           alert(data);
                           jQuery.each(data, function(i , item) {
                               $("#object").append('<option value="' + item.id + '">' + item.name + '</option>');
                           });
                         }
                     });
            }


        });




        $('#object').change(function(){
            reset();
            var id= $('#object').val();
            var product = $("#product").val();
            var sabaVersion = $("#sabaProduct").val();
            row=0;
            per=0;

            $.ajax({
                            url: '<%= request.getContextPath() %>/objects/getSavedMappings.view?object='+id +'&sabaproduct='+ product +'&sabaVersion=' + sabaVersion,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function(data){
                                if(data.length > 0)
                                {
                                    $("#savedMappings").show();
                                    $("#savedMappings").append('<option value="-1">Select Saved Mappings</option>');
                                    jQuery.each(data, function(i , item) {
                                        $("#savedMappings").append('<option value="' + item + '">' + item + '</option>');
                                    });


                                   $("#savedMappings").change(function(data)
                                   {
                                        var selectFile = $('#savedMappings option:selected').text();
                                        $.ajax({
                                           url: '<%= request.getContextPath() %>/objects/loadSavedMappings.view?object='+id +"&fileName=" + selectFile +"&sabaproduct="+product +"&sabaVersion=" + sabaVersion,
                                           processData: false,
                                           contentType: false,
                                           type: 'POST',
                                           success : loadSavedMappings
                                        });
                                   });

                                }

                            }
                        });

            $.ajax({
                url: '<%= request.getContextPath() %>/objects/getObject.view?id='+id,

                processData: false,

                contentType: false,
                type: 'POST',
                success: function(data){
                    html=data.length;
                     validateArray=[];
                    jQuery.each(data, function(i , item) {

                     if (item.required)
                           {
                            $("#data").append('<tr id=new' +(row) + '>'+
                                     '<td  id=saba' +(row)+' style="color:red" >' + item.fieldName +'</td>' +
                                     '<td > ' + '<select  id="candidateType'+(row)+'" name="status" class="input-medium fix-text-small">' + '</td>' +
                                     '<td >'  + '<input type="text" id=valu' +(row)+' />' + '</td>' +
                                     '<td >'  + '<input type="text" id=exper' +(row)+'>' + '</td>'                                     +'</tr>' )
                                     validateArray.push(row);
                            row++;
                    }
                    else {
                            $("#data").append('<tr id=new' +(row) + '>'+
                                      '<td  id=saba' +(row)+' >' + item.fieldName +'</td>' +
                                      '<td > ' + '<select id="candidateType'+(row)+'" name="status" class="input-medium fix-text-small">' + '</td>' +
                                      '<td >'  + '<input type="text" id=valu' +(row)+' />' + '</td>' +
                                      '<td >'  + '<input type="text" id=exper' +(row)+'>' + '</td>'                                     +'</tr>' )
                            row++;
                    }



                    });



                }
            });

        })
        $("button[id^='transform']").click(function(){

      for(var i=0;i<validateArray.length;i++)
      {
      var value=$('#candidateType'+validateArray[i]).val();
      if(value == -1){
      alert("Plese fill required fill");
      return;
      }

      }

        if( document.getElementById("filedata").files.length == 0 ){
                         console.log("no files selected");
                         alert("No File Selected! Please Select the file")
                         return;
             }
            var i=0;
            var array = [];
            while(row > i) {
            var mapfldObj = {
                             key : "",
                             name: "",
                             csvField: "",
                             default_val: ""
                             };
                           mapfldObj.key = $("#saba"+i).text();
                           mapfldObj.name = $("#saba"+i).text();
                           mapfldObj.csvField = $('#candidateType'+i).val();
                           var expesion = $('#exper'+i).val();
                           var sabaProduct = $('#product').val();
                           mapfldObj.default_val = $('#valu'+i).val();
                           array.push(mapfldObj);
                           i++;
            }
            var oMyForm = new FormData();
            oMyForm.append("file", filedata.files[0]);
            var delimitor=$('#delimitor').val();
            var product= $('#object option:selected').text();
            var id= $('#object').val();
            $.ajax({
                url: '<%= request.getContextPath() %>/objects/transform.view?delimitor='+delimitor + '&object='+product + '&ObjectKey='+id +'&mapping='+JSON.stringify(array),
                data: oMyForm,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    alert("Your Transformation is completed");
                    $('#csvlink').show();
                    $('#csvlink').html(data);
                }
            });
      });

       $("button[id^='save']").click(function(){

            var getfile = prompt("Please enter the file name:")
            if(getfile == null || getfile == ""){
                alert("File Name not given:");
                return;
            }
            saveMappingFile(getfile);
       });



        function saveMappingFile(getfileName)
        {

            var i=0;
            var array = [];
            while(row > i) {
           var mapfldObj = {
                             key : "",
                             name: "",
                             csvField: "",
                             default_val: ""
                             };
                mapfldObj.key = i;
                mapfldObj.name = $("#saba"+i).text();
                mapfldObj.csvField = $('#candidateType'+i).val();
                var expesion = $('#exper'+i).val();
                var sabaProduct = $('#product').val();
                var sabaVersion = $('#sabaProduct').val();
                mapfldObj.default_val = $('#valu'+i).val();
                array.push(mapfldObj);
                i++;
            }
            var oMyForm = new FormData();
            oMyForm.append("file", filedata.files[0]);
            var delimitor=$('#delimitor').val();
            var product= $('#object option:selected').text();
            var filename= getfileName;
            var id= $('#object').val();

            $.ajax({
                url: '<%= request.getContextPath() %>/objects/saveMapping.view?delimitor='+delimitor + '&object='+product + '&ObjectKey='+id +'&mapping='+JSON.stringify(array) + '&fileName='+filename +'&sabaProduct=' +sabaProduct + '&sabaVersion=' + sabaVersion + '&csvFieldListRestore='+ ((csvFieldListRestore=="" ||  csvFieldListRestore== null) ? "" : JSON.stringify(csvFieldListRestore)),
                data: oMyForm,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    alert("Saved Information");
                    $('#xmllink').show();
                    $('#xmllink').html(data);
                }
            });
      }


        $("#filedata").change(function(){
            var oMyForm = new FormData();
            oMyForm.append("file", filedata.files[0]);
            var id=$('#delimitor').val();

            if(id == "" || id == null){
                alert("Please select Delimitor and then select file ");
                $("#filedata").val("");
                return false;
            }


            $.ajax({
                url: '<%= request.getContextPath() %>/objects/getCustomerFields.view?id='+id,
                data: oMyForm,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                per=0;
                 while(html > per)
                 {
                               $("#candidateType" +(per)).html("");
                               getDropDownValues(data[0], "#candidateType" +(per));
                               per++;
                 }

            }
                  



                });
            });




        function getDropDownValues(list, typeId){
            var j=0;

            var html = '<option value="-1">Select</option>';

            jQuery.each(list, function(i , item) {
                html += '<option value="'+j+'">'+item+'</option>';
                j++;
            });
            jQuery(typeId).append(html);


        }

    });
</script>
</head>
<body onload="fnActivateNavigation('campaignListTab')">
<jsp:include page="/template/header.jsp" />
<div class="container">
    <div class="row">
        <form class="form-search" action="<%= request.getContextPath() %>/campaigns/getCustomerFields.view" id="frmDatamapListing" enctype="multipart/form-data" method="POST" >
            <fieldset>
                <legend>Design Mapping</legend>
                <label class="control-label" >Saba Product:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select id="product" name="sabaProduct" class="input-medium">
                    <c:forEach items="${object1}" var="keyword">
                        <option value=<c:out value="${keyword.id}"/> ><c:out value="${keyword.name}"/></option>
                    </c:forEach>
                </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </fieldset>
            <fieldset>
                <label id="version" class="control-label" style="padding-left: 52px; padding-top: 20px; margin-left: -54px;">Target:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select id="sabaProduct"  name="sabaProduct1" class="input-medium" style="margin-left: 43px;">
                    <c:forEach items="${sabaversion}" var="keyword1">
                       <option value=<c:out value="${keyword1}"/> ><c:out value="${keyword1}"/></option>
                    </c:forEach>''
                 </select>
            </fieldset>
            <br/>
            <fieldset>
                <label class="control-label" >Objects:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select id="object" name="objectProduct" class="input-medium" placeholder="Object Name">
                    <option default>Object Name</option>
                    <c:forEach items="${object}" var="keyword">
                        <option value=<c:out value="${keyword.id}"/> ><c:out value="${keyword.name}"/></option>
                    </c:forEach>
                </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select id="savedMappings"  class="input-medium" role="menu" style="margin-left: 21px; border-color: black;">
                 </select>
            </fieldset>
            <br/>
            <fieldset>
                <label class="control-label" for="delimitor" ><spring:message code="label.object.delimitor"/></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" id="delimitor" placeholder="Delimitor"  validation="@Required" name="delimitor" class="input-medium" style="margin-left: -35px;" />
            </fieldset>
            <br/>
            <fieldset>
                <label class="control-label" >Upload File:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="file" name="filedata"  validation="@Required" id="filedata"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" id="save" >Save mapping </button>&nbsp;&nbsp;&nbsp;
                <button type="button"  class="btn btn-primary" id="transform" >Transform</button>
            </fieldset>
            <br/>
            <br/>
            <table id = "data" class="table table-nonfluid table-bordered table-striped" style="margin-bottom: 50px;">
                <thead>
                <tr>
                    <th>Source Fields</th>
                    <th>CSV Fields</th>
                    <th>Default Value</th>
                    <th>Expression</th>
                </tr>
                </thead>

            </table>
            <fieldset style="margin-left: 471px;">
                <button type="button" class="btn btn-primary" id="save1" style="margin-bottom: 30px;">Save mapping </button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" id="transform1" style="margin-bottom: 30px;">Transform</button>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>
