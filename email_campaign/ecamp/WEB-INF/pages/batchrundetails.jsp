<%@ page import="java.util.List" %>
<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
</head>
<body  onload="fnActivateNavigation('monitorListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <table class="table table-bordered table-condensed table-striped">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><spring:message code="label.user.email"/></th>
                <th><spring:message code="label.mailserver.fromAddress"/></th>
                <th><spring:message code="label.template.name"/></th>
            </tr>
        </thead>
        <tbody>
            <% int i=1; %>
            <c:forEach items="${recipients}" var="item">
                <tr>
                    <td><%= i%></td>
                    <td><c:out value="${item.email}"/></td>
                    <td><c:out value="${item.fromEmail}"/></td>
                    <td><c:out value="${item.templateName}"/></td>
                </tr>
                <% i++; %>
            </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>