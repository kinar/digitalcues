<%@ page import="com.tola.platform.core.TolaException" %>
<%@ page import="com.tola.platform.exception.PlatformExceptionMessage" %>
<%@ page isErrorPage = "true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body >
<%
    if(exception instanceof TolaException){

        if(((TolaException)exception).getExceptionMessage().getId().equals(PlatformExceptionMessage.USER_SESSION_EXPIRED.getId())){


        }
    }
%>



    <h2>Your application has generated an error</h2>
    <h3>Please check for the error given below</h3>
    <b>Exception:</b><br>
    <font color="red"><%= exception.getMessage() %></font>
</body>
</html>