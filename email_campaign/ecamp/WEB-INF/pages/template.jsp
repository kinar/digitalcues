<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <script type="text/javascript">
        tinymce.init({
            menubar: false,
            selector: "textarea"
        });

        function fnGotoTemplateSearch(){
            document.forms['frmTemplate'].action="<%= request.getContextPath()%>/template/templateList.view";
            document.forms['frmTemplate'].method="GET";
            document.forms['frmTemplate'].submit();
       }
    </script>
</head>
<body onload="fnActivateNavigation('templateListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <div class="row">
        <form class="form-horizontal" action="<%= request.getContextPath() %>/template/savetemplate.view" method="POST" id="frmTemplate">
            <fieldset>
                <legend><spring:message code="label.template.templateDetails"/> </legend>
                <div class="control-group">
                    <label class="control-label" for="name"><spring:message code="label.template.name"/></label>
                    <div class="controls">
                        <input type="text" id="name" placeholder="<spring:message code="label.template.name"/>" name="name" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="subject"><spring:message code="label.template.subject"/></label>
                    <div class="controls">
                        <input type="text" id="subject" placeholder="<spring:message code="label.template.subject"/>" name="subject" class="input-xlarge" value="${form.bean.subject}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="body"><spring:message code="label.template.body"/></label>
                    <div class="controls">
                        <textarea id="body"
                                  placeholder="<spring:message code="label.template.body"/>"
                                  name="body" class="input-medium">${form.bean.body}</textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="status"><spring:message code="label.template.status"/>:</label>
                    <div class="controls">
                        <select id="status" name="status" class="input-medium">
                            <option value="100" <c:if test="${form.bean.status eq '100'}">selected</c:if>><spring:message code="label.template.active"/></option>
                            <option value="200" <c:if test="${form.bean.status eq '200'}">selected</c:if>><spring:message code="label.template.inactive"/></option>
                        </select>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary"><spring:message code="label.master.save"/></button>
                    <button type="button" class="btn" onclick="javascript:fnGotoTemplateSearch()"><spring:message code="label.master.cancel"/></button>
                </div>
                <input type="hidden" id="id" name="id" value="${form.bean.id}"/>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>