<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Map.Entry"%>
<%@ page import="java.net.URLEncoder" %>

<link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet">
<link href="<%= request.getContextPath() %>/css/datepicker.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/bootstrap-responsive.css" rel="stylesheet">
<script src="<%= request.getContextPath()%>/js/jquery1.10.2-min.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap-transition.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/tinymce/tinymce.min.js"></script>
<script src="<%= request.getContextPath()%>/js/validate-min.jsp"></script>

<%
    TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
    if(properties==null)
        properties = new TableProperties();
     Map searchBy = properties.getSearchBy();
     Map sortInfo = properties.getSortInfo();
     int pgs=properties.getTotalPages();
     int col=properties.getCurrentPage();
     String searchQuery = "";
     if(searchBy!=null && !searchBy.isEmpty()){
         for(Object key: searchBy.keySet()){
             String k = (String)key;
             System.out.println(""+k);
             String value  = (String)searchBy.get(k);
             System.out.println(""+value);
             searchQuery = searchQuery +"&" + k + "=" + URLEncoder.encode(value);
             System.out.println(""+searchQuery);
         }
         searchQuery = searchQuery.substring(1);
     }
     if(sortInfo!=null && !sortInfo.isEmpty()){
         for(Object key: sortInfo.keySet()){
             String k = (String)key;
             System.out.println(""+k);
             String value  = (String)sortInfo.get(k);
             System.out.println(""+value);
             String sortOrder = "asc".equalsIgnoreCase(value)?"DESC":"ASC";
             searchQuery = searchQuery +"&column=" + k  + "&sortOrder=" +  sortOrder;
             System.out.println(""+searchQuery);
         }
     }

%>
<center>
<div class="row">
    <div class="span3">

         <p align="right"><button type="button" class="btn" onclick="javascript:prvpage()"><i class="icon-arrow-left"></i></button>
        <span id="lbl"><%=col%></span> of<span id="lbl1">  <%=  properties.getTotalPages() %></span>
        <button type="button" class="btn" onclick="javascript:nxtpage()"><i class="icon-arrow-right"></i></button></p>
    </div>

<div class="span3">
    <p align="left">
    <select class="span1"  id="myselect" onchange="goto()" >
        <option>Go</option>
        <c:forEach var="pgs" begin="1" end="<%=pgs%>">
            <option> ${pgs} </option>
        </c:forEach>
    </select>
    </p>
</Div>
</center>


<script>

    function nxtpage(){
        var id= document.getElementById("lbl").innerHTML;
        var cnt= document.getElementById("lbl1").innerHTML;
        if(parseInt(<%=col%>) < parseInt(cnt))
        { document.getElementById("lbl").innerHTML=parseInt(id)+1;
            window.location.assign(getControllerURL() + "?<%= searchQuery %>" + "&page=" + (parseInt(id)+1));
        }
    }

    function prvpage(){
        var id= document.getElementById("lbl").innerHTML;
        if(parseInt(<%=col%>)>1)
        {
            document.getElementById("lbl").innerHTML=parseInt(id)-1;
            window.location.assign(getControllerURL() +"?<%= searchQuery %>"+ "&page=" + (parseInt(id)-1));
        }
    }

    function getControllerURL(){
        var url = window.location.href;
        console.log(url);
        return url.substring(0, url.indexOf('?') );
    }
    function goto(){
        if(document.getElementById("myselect").value!="Go")
            window.location.assign(getControllerURL() +"?<%= searchQuery %>" + "&page=" + (document.getElementById("myselect").value));
    }
</script>