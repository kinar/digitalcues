<%@ page import="com.tola.platform.ServiceLocator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <%
        boolean isAdmin= ServiceLocator.getInstance().getPrincipal().isAdmin();
    %>
</head>
<body onload="<% if(isAdmin){ %>fnActivateNavigation('userListTab')<%}else{ %>fnActivateNavigation('changePasswordTab')<% } %>">
<jsp:include page="/template/header.jsp" />



        <div class="row">
            <form class="form-horizontal" action="<%= request.getContextPath() %>/user/saveuseradd.view" method="POST"
                  id="frmUser">
                <fieldset>
                    <legend><spring:message code="label.user.userDetails"/> </legend>
                    <div class="control-group">
                        <label class="control-label" for="firstname"><spring:message code="label.user.firstName"/></label>
                        <div class="controls">
                            <input type="text" id="firstname" placeholder="<spring:message code="label.user.firstName"/>" name="firstName" class="input-xlarge" value="${form.bean.firstName}" validation="@Required @Length(min=1,max=25)">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="lastname"><spring:message code="label.user.lastName"/></label>
                        <div class="controls">
                            <input type="text" id="lastname" placeholder="<spring:message code="label.user.lastName"/>" name="lastName" class="input-xlarge" value="${form.bean.lastName}" validation="@Required @Length(min=1,max=25)">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email"><spring:message code="label.user.email"/></label>
                        <div class="controls">
                            <input type="text" id="email" placeholder="<spring:message code="label.user.email"/>" name="email" class="input-xlarge" value="${form.bean.email}" validation="@Required @Length(min=1,max=50) @Email">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="username"><spring:message code="label.user.userName"/></label>
                        <div class="controls">
                            <input type="text" id="username" placeholder="<spring:message code="label.user.userName"/>" name="username"  class="input-xlarge" value="${form.bean.username}" validation="@Required @Length(min=1,max=25)">
                        </div>
                    </div>
                  <%--  <% if(ServiceLocator.getInstance().getPrincipal().isAdmin()){%>--%>
                    <%--<div class="control-group">
                        <label class="control-label" for="role"><spring:message code="label.user.userRole"></spring:message></label>
                        <div class="controls">
                            <select id="role" name="role" class="input-medium">
                                <option value="100" <c:if test="${form.bean.role eq '100'}">selected</c:if>><spring:message code="label.user.user"></spring:message></option>
                                <option value="200" <c:if test="${form.bean.role eq '200'}">selected</c:if>><spring:message code="label.user.superuser"></spring:message></option>
                            </select>
                        </div>
                    </div>
--%>
                    <%--<div class="control-group">
                        <label class="control-label" for="status"><spring:message code="label.user.userStatus"/></label>
                        <div class="controls">
                            <select id="status" name="status" class="input-medium">
                                <option value="100" <c:if test="${form.bean.status eq '100'}">selected</c:if>><spring:message code="label.user.active"/></option>
                                <option value="200" <c:if test="${form.bean.status eq '200'}">selected</c:if>><spring:message code="label.user.inactive"/></option>
                            </select>
                        </div>
                    </div>--%>
                   <%-- <%}else{%>
                    <input type="hidden" id="status" name="status" value="<c:out value="${form.bean.status}"/>"/>
                    <%} %>

                    <input type="hidden" id="id" name="id" value="${form.bean.id}"/>--%>
                    <div class="form-actions">
                        <button type="submit"  class="btn btn-primary"><spring:message code="label.master.save"/></button>
                        <button type="button" class="btn"><spring:message code="label.master.cancel"/></button>
                    </div>
                </fieldset>
            </form>
        </div>



</body>
</html>