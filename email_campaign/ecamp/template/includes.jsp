<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet">
<link href="<%= request.getContextPath() %>/css/datepicker.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/bootstrap-responsive.css" rel="stylesheet">
<script src="<%= request.getContextPath()%>/js/jquery1.10.2-min.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap-transition.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>
<script src="<%= request.getContextPath()%>/js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/tinymce/tinymce.min.js"></script>
<script src="<%= request.getContextPath()%>/js/validate-min.jsp"></script>

<style>
    .errorblock {
        color: #ff0000;
        background-color: #ffEEEE;
        border: 3px solid #ff0000;
        padding: 8px;
        margin: 16px;
    }
    .table-nonfluid {
        width: auto;
    }

    #footer {
        height: 30px;
        background-color: #F5F5F5;
    }
    body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
    }
</style>
<script>
    function fnActivateNavigation(navId){
        $('#topnav li').removeAttr("class");
        $('#' + navId).attr("class","active");
        $('.datepicker').datepicker({
            format:'mm/dd/yyyy'
           }
        ).on('changeDate',function(){
           $('.datepicker').datepicker('hide');
        });
    }
</script>
<title><spring:message code="label.application.name"/></title>