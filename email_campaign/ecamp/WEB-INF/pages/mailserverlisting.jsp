<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ page import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <%
        TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
        if(properties==null)

            properties = new TableProperties();

        Map searchBy = properties.getSearchBy();
        Map sortInfo = properties.getSortInfo();
        int rows = properties.getTotalrows();
        int col=properties.getCurrentPage();
        int begin=properties.getBegin();
        int end=properties.getEnd();
        int pgs=properties.getTotalPages();

    %>
    <script>
        function fnGotoNewMailServer(){
            document.forms['frmMailServerSearch'].action="<%= request.getContextPath()%>/mailserver/newmailserver.view";
            document.forms['frmMailServerSearch'].submit();
        }

        function fnSort(col, order){
            var sortBy = "";
            if(order=='null' || order==null)
                sortBy = 'ASC';
            else
                sortBy =order;
            window.location.assign("<%= request.getContextPath() %>/mailserver/searchmailserver.view?fromAddress="+ escape($('#fromAddress').val()) +"&host="+ escape($('#host').val())+ "&column=" + col + "&sortOrder=" + sortBy);
        }

        function fnGotoEditMailServer(mailServerId){
            document.forms['frmEditMailServer'].action="<%= request.getContextPath()%>/mailserver/editmailserver.view";
            $('#id').attr('value',mailServerId);
            document.forms['frmEditMailServer'].submit();
        }

        function goto(){
            if(document.getElementById("myselect").value!="Go")
                window.location.assign("<%= request.getContextPath() %>/mailserver/searchmailserver.view?fromAddress="+ escape($('#fromAddress').val()) +"&host="+ escape($('#host').val()) + "&page=" + (document.getElementById("myselect").value));
        }
    </script>
</head>
<body onload="fnActivateNavigation('mailserverListTab')">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <div class="row">
        <form class="form-search" action="<%= request.getContextPath() %>/mailserver/searchmailserver.view" method="GET" id="frmMailServerSearch">
            <fieldset>
                <legend><spring:message code="label.mailserver.searchMailServer"/></legend>
                    <input type="text" class="input-medium" placeholder="<spring:message code="label.mailserver.fromAddress"/>" name="fromAddress" id="fromAddress"
                           value="<%= searchBy.get("fromAddress")==null?"":searchBy.get("fromAddress") %>"
                            validation="@OptionalRequired(fields=fromAddress,host)" label="<spring:message code="label.mailserver.fromAddress"/>">

                <input type="text" class="input-medium" placeholder="<spring:message code="label.mailserver.host"/>" name="host" id="host"
                       value="<%= searchBy.get("host")==null?"":searchBy.get("host") %>" label="<spring:message code="label.mailserver.host"/>">

                    <button type="button" class="btn" onclick="javascript:validateForm('frmMailServerSearch')"><i class="icon-search"></i></button>
                    <button type="button" class="btn" onclick="javascript:fnGotoNewMailServer()"><spring:message code="label.master.new"/></button>
            </fieldset>
        </form>
    </div>
    <div class="row">
        <c:if test="${showtable=='Y'}">
            <c:choose>
                <c:when test="${empty mailservers}">
                    <div class="alert span6"><spring:message code="message.nodatafound"/></div>
                </c:when>
                <c:otherwise>
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th><a href="javascript:fnSort('FROM_ADDRESS','<%= sortInfo.get("FROM_ADDRESS") %>')">
                                    <spring:message code="label.mailserver.fromAddress"/>
                                    <% if(properties.getSortClass("FROM_ADDRESS")!=null){%>
                                    <i id="myb" class="<%= properties.getSortClass("FROM_ADDRESS")%>"></i>
                                    <% }%>
                                </a>
                                </th>
                                <th>
                                    <a href="javascript:fnSort('HOST_NAME','<%= sortInfo.get("HOST_NAME") %>')">
                                    <spring:message code="label.mailserver.host"/>
                                    <% if(properties.getSortClass("HOST_NAME")!=null){%>
                                    <i id="myb" class="<%= properties.getSortClass("HOST_NAME")%>"></i>
                                    <% }%>
                                </th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="mailServer" items="${mailservers}" begin="<%=begin%>" end="<%=end%>">
                                <tr>
                                    <td><c:out value="${mailServer.fromAddress}"/></td>
                                    <td><c:out value="${mailServer.host}"/></td>
                                    <td><a href="javascript:fnGotoEditMailServer('<c:out value="${mailServer.id}"/>')"><spring:message code="label.master.edit"/></a></td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="3">
                                    <jsp:include page="/template/pagination.jsp"></jsp:include>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>
        </c:if>
    </div>
</div>
<form id="frmEditMailServer" method="POST">
    <input id="id" name="id" type="hidden"/>
</form>
</body>
</html>