<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ page import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <jsp:include page="/template/includes.jsp" />
    <%
        TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
        if(properties==null)
            properties = new TableProperties();

        Map searchBy = properties.getSearchBy();
        Map sortInfo = properties.getSortInfo();
        int rows = properties.getTotalrows();
        int col=properties.getCurrentPage();
        int begin=properties.getBegin();
        int end=properties.getEnd();
        int pgs=properties.getTotalPages();
       response.setHeader("Refresh", "10; URL=http://localhost:8080/ecamp/index.view");


    %>
    <script>

        function fnSort(col, order){
            var sortBy = "";
            if(order=='null' || order==null)
                sortBy = 'ASC';
            else
                sortBy =order;
            window.location.assign("<%= request.getContextPath() %>/index.view?campaignName="+escape($('#campaignName').val())+ "&column=" + escape(col) + "&sortOrder=" + escape(sortBy));
        }
    </script>
</head>
<body onload="fnActivateNavigation('monitorListTab')">
<jsp:include page="/template/header.jsp" />
<div class="container">
    <div class="row">
        <form class="form-search" action="<%= request.getContextPath() %>/index.view" method="GET" id="frmTemplateSearch">
            <fieldset>
                <legend><spring:message code="label.campaign.searchCampaign"/></legend>
                <input type="text" class="input-medium" placeholder="<spring:message code="label.campaign.name"/>" name="campaignName" id="campaignName" value="<%=searchBy.get("campaignName").toString().equalsIgnoreCase("%")?"":searchBy.get("campaignName")%>">

                <button type="submit" class="btn"><i class="icon-search"></i></button>
            </fieldset>
        </form>
    </div>
<div class="span8">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th> <a href="javascript:fnSort('c.name','<%= sortInfo.get("c.name") %>')">
                    <spring:message code="label.campaign.name"/>
                    <% if(properties.getSortClass("c.name")!=null){%>
                    <i id="myb" class="<%= properties.getSortClass("c.name")%>"></i>
                    <% }%>
                    </a>
                </th>

                <th>
                    <a href="javascript:fnSort('s.start_date','<%= sortInfo.get("s.start_date") %>')">
                    <spring:message code="label.schedule.startDate"/>
                        <% if(properties.getSortClass("s.start_date")!=null){%>
                        <i id="myb" class="<%= properties.getSortClass("s.start_date")%>"></i>
                        <% }%>
                    </a>
                </th>
                <th><spring:message code="label.campaign.totalRecipients"/></th>
                <th><spring:message code="label.campaign.sentRecipients"/></th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${campaigns}" var="campaign" begin="<%=begin%>" end="<%=end%>">
                <c:set value="${campaign.startDate}" var="startDate"/>
                <tr>
                    <td><c:out value="${campaign.name}"/></td>
                    <td><fmt:formatDate type="date" value="${startDate}" pattern="MM/dd/yyyy"/> </td>
                    <td><c:out value="${campaign.totalRecipients}"/></td>
                    <td><c:out value="${campaign.sentRecipients}"/></td>
                    <td><c:choose>
                            <c:when test="${campaign.status ==300}">
                                <a href="<%= request.getContextPath()%>/scheduler/pausecampaign.view?id=<c:out value='${campaign.id}'/>"><spring:message code="label.campaign.pause"/></a>
                            </c:when>
                            <c:when test="${campaign.status ==500}">
                                <a href="<%= request.getContextPath()%>/scheduler/resumecampaign.view?id=<c:out value='${campaign.id}'/>"><spring:message code="label.campaign.resume"/></a>
                            </c:when>
                        <c:when test="${campaign.status ==100}">
                            <a href="<%= request.getContextPath()%>/scheduler/stopcampaign.view?id=<c:out value='${campaign.id}'/>">Stop</a>
                        </c:when>
                        </c:choose>&nbsp;
                        <a href="<%= request.getContextPath()%>/campaign/detailscampaign.view?id=<c:out value='${campaign.id}'/>"><spring:message code="label.campaign.details"/></a>
                    </td>
                </tr>
            </c:forEach>
            <tr>
                <td colspan="5">
                    <jsp:include page="/template/pagination.jsp"></jsp:include>
                </td>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>