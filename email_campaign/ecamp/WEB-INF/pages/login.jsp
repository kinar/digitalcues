<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
</head>
<body onload='document.loginForm.j_username.focus();'>
<jsp:include page="/template/headerlogin.jsp" />
<div class="row">
<div class="span3">&nbsp;</div>
<div class="span6">
<img src="<%= request.getContextPath()%>/img/harsh.png"/>
<form id='frmLogin'
      name="loginForm"
      action="<c:url value='j_spring_security_check' />"
      class="form-horizontal"
      method='POST'>
    <fieldset>
        <legend>
            <spring:message code="label.login.welcome"/></legend>
            <div class="control-group">
                <label class="control-label" for="username"><spring:message code="label.login.username"/></label>
                <div class="controls">
                    <input type="text" id="username" placeholder="username" name="j_username" validation="@Required @Length(min=1,max=25)" onkeypress="if (event.keyCode == 13) document.getElementById('loginButton').click()">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="password"><spring:message code="label.login.password"/></label>
                <div class="controls">
                    <input type="password" id="password" placeholder="password" name="j_password" validation="@Required @Length(min=1,max=25)" onkeypress="if (event.keyCode == 13) document.getElementById('loginButton').click()">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button onclick="javascript:validateForm('frmLogin')" id="loginButton" name="login" type="button" class="btn btn-primary"><spring:message code="label.login"/></button>
                    <button name="reset" type="reset" class="btn"><spring:message code="label.login.reset"/></button>
                </div>
            </div>
    </fieldset>
</form>
</div>

</div>

<jsp:include page="/template/footer.jsp"/>
</body>
</html>