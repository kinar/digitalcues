<%@ page import="java.util.List" %>
<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <jsp:include page="/template/includes.jsp" />
    <script>
        function fnShowRecipient(campaignId,status){
            window.open('<%= request.getContextPath()%>/campaign/recipientlisting.view?id=' + campaignId + '&status=' + status);
        }
    </script>
</head>
<body onload="fnActivateNavigation('campaignListTab');">
<jsp:include page="/template/header.jsp" />

<div class="container">
    <jsp:include page="/template/campaigninclude.jsp"/>
            <span class="label label-important"><spring:message code="labels.campaign.recipient.eraseWarning"/></span>
            <form class="form-horizontal" method="POST" id="frmCampaignRecipient" enctype="multipart/form-data" action="<%= request.getContextPath()%>/campaign/recipientsave.view">
                <fieldset>
                    <legend><spring:message code="label.campaign.recipientDetails"/> </legend>
                    <input type="hidden" name="campaignId" value="${form.bean.ownerId}"/>
                    <div class="control-group">
                        <label class="control-label" for="fileSelect"><spring:message code="label.campaign.uploadRecipient"/></label>
                        <div class="controls">
                            <input type="file" name="filedata" validation="@Required" id="fileSelect"/>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn btn-primary" onclick="javascript:validateForm('frmCampaignRecipient')"><spring:message code="label.master.save"/></button>
                        <div class="btn-group">
                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                <spring:message code="label.master.show"/>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:fnShowRecipient('${form.bean.ownerId}','F')"><spring:message code="label.master.showFailure"/></a></li>
                                <li><a href="javascript:fnShowRecipient('${form.bean.ownerId}','S')"><spring:message code="label.master.showSuccess"/></a></li>
                                <li><a href="javascript:fnShowRecipient('${form.bean.ownerId}','A')"><spring:message code="label.master.showAll"/></a></li>
                            </ul>
                        </div>
                        <c:if test="${form.bean.attachedTemplates}">
                            <a  class="btn"
                                href="<%= request.getContextPath()%>/campaign/campaigntemplate.view?id=<c:out value="${form.bean.ownerId}"/>" ><spring:message code="label.campaign.addTemplate"/></a>
                        </c:if>
                        <button type="button" class="btn" onclick="javascript:fnGotoCampaignSearch('frmCampaignRecipient')"><spring:message code="label.master.cancel"/></button>

                    </div>
                </fieldset>
            </form>
</div>
</body>
</html>