<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.tola.client.security.SecurityUtil" %>
<%@ page import="com.tola.platform.ServiceLocator" %>
<%
    String loggedInUser = ServiceLocator.getInstance().getPrincipal().getId();
    String user = ServiceLocator.getInstance().getPrincipal().getName();
%>
<style>

    .logo
    {


        top:40px;
    }

</style>
<div class="navbar navbar-fixed-top">

    <div  id="topNavbar">
        <div class="span1">
            <img src="<%= request.getContextPath()%>/img/Digital_Cues3.png" style="height:40px"/>
        </div>

        <div class="navbar-inner container-fluid">

            <a class="brand"><spring:message code="label.master.logo"/></a>

            <div class="nav-collapse collapse">
                <ul id="topnav" class="nav" id="topNavTabs">
                    <% if(SecurityUtil.isAdmin()){ %>
                    <li id="campaignListTab"><a href="<%= request.getContextPath()%>/objects/getAllObjects.view">Design</a></li>
                    <li id="userListTab"><a href="<%= request.getContextPath()%>/admin/userList.view"><spring:message code="label.header.users"/></a></li>
                    <li id="changePasswordTab"><a href="<%= request.getContextPath()%>/admin/showpassword.view"><spring:message code="label.header.password"/></a></li>
                    <%}else{ %>
                    <%--<li id="monitorListTab"><a href="<%= request.getContextPath()%>/index.view"><spring:message code="label.header.monitor"/></a></li>--%>
                    <li id="campaignListTab"><a href="<%= request.getContextPath()%>/objects/getAllObjects.view">Design</a></li>
                    <%-- <li id="templateListTab"><a href="<%= request.getContextPath()%>/template/templateList.view"><spring:message code="label.header.template"/></a></li>
                     <li id="mailserverListTab"><a href="<%= request.getContextPath()%>/mailserver/mailserverList.view"><spring:message code="label.header.mailserver"/></a></li>
                    --%> <li id="changePasswordTab"><a href="<%= request.getContextPath()%>/user/edituser.view?id=<%= loggedInUser%>"><spring:message code="label.header.profile"/></a></li>
                    <%} %>
                    <li id="logoutTab"><a href='<c:url value="/j_spring_security_logout" />'><spring:message code="label.header.logout"/></a></li>


                </ul>
            </div><!--/.nav-collapse --><div align="center"  class="logo" style="padding-top: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font size="4"> Welcome &nbsp;<%=user%></font></b></div>
        </div>



        </div>

</div>
<div class="row">&nbsp;</div>
<div class="container">
    <c:if test="${not empty form.feedback}">
    <span class="alert alert-success span10" style="z-index: 999">
            <spring:message code="${form.feedback}"/>
    </span>
    </c:if>
    <c:if test="${not empty form.exception}">
    <span class="alert alert-error span10">
            ${form.exception.message}
    </span>
    </c:if>
    <div class="row">&nbsp;</div>

    <div id="footer" class="topNavbar navbar-fixed-bottom navbar-inverse">&#169;<spring:message code="label.campaign.footer"/></div>

