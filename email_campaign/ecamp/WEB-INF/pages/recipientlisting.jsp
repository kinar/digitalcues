<%@ page import="com.tola.client.platform.TableProperties" %>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.List" %>
<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <jsp:include page="/template/includes.jsp" />
    <%
        TableProperties properties = (TableProperties)request.getAttribute("tableProperties");
        if(properties==null)

            properties = new TableProperties();

        Map searchBy = properties.getSearchBy();
        Map sortInfo = properties.getSortInfo();
        int rows = properties.getTotalrows();
        int col=properties.getCurrentPage();
        int begin=properties.getBegin();
        int end=properties.getEnd();
        int pgs=properties.getTotalPages();

    %>
    <script>

        function nxtpage(){
            var id= document.getElementById("lbl").innerHTML;
            var cnt= document.getElementById("lbl1").innerHTML;
            if(parseInt(<%=col%>) < parseInt(cnt))
            { document.getElementById("lbl").innerHTML=parseInt(id)+1;

                window.location.assign("<%= request.getContextPath() %>/recipientlisting.view?id="  + <%= searchBy.get("id") %>  + "&status="+ <%=searchBy.get("status")%> + "&page=" + (parseInt(id)+1));
            }
        }

        function prvpage(){
            var id= document.getElementById("lbl").innerHTML;
            if(parseInt(<%=col%>)>1)
            {
                document.getElementById("lbl").innerHTML=parseInt(id)-1;
                window.location.assign("<%= request.getContextPath() %>/recipientlisting.view?id="  + <%= searchBy.get("id") %>  + "&status="+ <%=searchBy.get("status")%> + "&page=" + (parseInt(id)-1));
            }
        }
        function goto(){
            if(document.getElementById("myselect").value!="Go")
                window.location.assign("<%= request.getContextPath() %>/recipientlisting.view?id="  + <%= searchBy.get("id") %>  + "&status="+ <%=searchBy.get("status")%> + "&page=" + (document.getElementById("myselect").value));
        }

    </script>
</head>
<body>
    <table class="table table-nonfluid table-bordered table-striped">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><spring:message code="label.campaign.recipient.email"/></th>
                <th><spring:message code="label.campaign.recipient.friendlyName"/></th>
                <c:forEach items="${keywords}" var="keyword">
                    <th><c:out value="${keyword.keywordName}"/></th>
                </c:forEach>
                <th><spring:message code="label.campaign.recipient.message"/></th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${form.bean}" var="item" begin="<%=begin%>" end="<%=end%>">
            <tr>
                <td><c:choose>
                        <c:when test="${item.inValid=='1'}">
                            <img src="<%= request.getContextPath()%>/img/red.png">
                        </c:when>
                        <c:otherwise>
                            <img src="<%= request.getContextPath()%>/img/green.png"/>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td><c:out value="${item.email}"/></td>
                <td><c:out value="${item.friendlyName}"/></td>
                <c:forEach items="${item.keywords}" var="kValue">
                     <td><c:out value="${kValue.value}"/></td>
                </c:forEach>
                <td><c:out value="${item.validationMessage}"/></td>
                <td><a href="<%= request.getContextPath()%>/campaign/recipientdelete.view?id=<c:out value='${item.id}'/>&campaignId=<c:out value='${item.campaignId}'/>&status=<c:out value='${status}'/>"><spring:message code="label.master.delete"/></a></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="15">
                <jsp:include page="/template/pagination.jsp"></jsp:include>
            </td>
        </tr>
        </tbody>
    </table>
</body>
</html>