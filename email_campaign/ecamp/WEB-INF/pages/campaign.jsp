<%@ page import="java.util.List" %>
<%@ page import="com.tola.dto.Breadcrumb" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.tola.client.platform.FormBean" %>
<%@ page import="com.tola.dto.BreadcrumbUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/template/includes.jsp" />
    <script>
       function fnSaveCampaign(status){
           $('#campaignStatus').attr("value",status);
           validateForm('frmCampaign');
       }
    </script>
</head>
<body onload="fnActivateNavigation('campaignListTab');">
<jsp:include page="/template/header.jsp" />
<div class="container">
    <jsp:include page="/template/campaigninclude.jsp"/>
        <div class="tab-pane active" id="homeTab">
            <form class="form-horizontal" method="POST" id="frmCampaign" action="<%= request.getContextPath()%>/campaign/savecampaign.view">
                <fieldset>
                    <legend><spring:message code="label.campaign.campaignDetails"/> </legend>
                    <div class="control-group">
                        <label class="control-label" for="campaignName" ><spring:message code="label.campaign.name"/></label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.campaign.name"/>" name="campaign.name" class="input-xlarge" value="${form.bean.campaign.name}"
                                   id="campaignName" validation="@Required" maxlength="100">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="campaignDescription"><spring:message code="label.campaign.description"/></label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.campaign.description"/>" name="campaign.description" class="input-xlarge" value="${form.bean.campaign.description}"
                                    id="campaignDescription" maxlength="255">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" ><spring:message code="label.campaign.status"/></label>
                        <div class="controls">
                            <c:choose>
                                <c:when test="${form.bean.campaign.status=='100'}">
                                    <spring:message code="label.campaign.active"/>
                                </c:when>
                                <c:when test="${form.bean.campaign.status=='200'}">
                                    <spring:message code="label.campaign.inactive"/>
                                </c:when>
                                <c:when test="${form.bean.campaign.status=='300'}">
                                    <spring:message code="label.campaign.inProgress"/>
                                </c:when>
                                <c:when test="${form.bean.campaign.status=='400'}">
                                    <spring:message code="label.campaign.complete"/>
                                </c:when>
                                <c:when test="${form.bean.campaign.status=='500'}">
                                    <spring:message code="label.campaign.paused"/>
                                </c:when>
                            </c:choose>
                        </div>
                        <input type="hidden" name="campaign.status" id="campaignStatus" value="<c:out value="${form.bean.campaign.status}"/>" validations="@Required"/>
                    </div>
                    <div class="form-actions">
                        <c:choose>
                            <c:when test="${form.bean.campaign.status=='100'}">
                                <button type="button" class="btn btn-primary" onclick="javascript:fnSaveCampaign('200')"><spring:message code="label.master.deactivate"/></button>
                            </c:when>
                            <c:when test="${form.bean.campaign.status=='200' && canActivate=='Y'}">
                                <button type="button" class="btn btn-primary" onclick="javascript:fnSaveCampaign('100')"><spring:message code="label.master.activate"/></button>
                            </c:when>
                        </c:choose>
                        <button type="button" class="btn btn-primary" onclick="javascript:validateForm('frmCampaign')"><spring:message code="label.master.save"/></button>
                        <c:if test="${form.bean.attachedRecipients}">
                            <a  class="btn"
                                href="<%= request.getContextPath()%>/campaign/recipientupload.view?id=<c:out value="${form.bean.ownerId}"/>" ><spring:message code="label.campaign.addRecipient"/></a>
                        </c:if>
                        <button type="button" class="btn" onclick="javascript:fnGotoCampaignSearch('frmCampaign')"><spring:message code="label.master.cancel"/></button>

                    </div>
                    <input type="hidden" id="id" name="campaign.id" value="${form.bean.campaign.id}"/>
                </fieldset>
            </form>

    </div>
</div>
</body>
</html>