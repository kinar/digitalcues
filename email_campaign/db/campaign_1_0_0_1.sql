alter table CPN_DISTRIBUTION_LIST add COLUMN FRIENDLY_NAME VARCHAR(255);

alter table CPN_MAIL_PROPERTY add column  FRIENDLY_NAME  VARCHAR (255);

alter table CPN_CAMPAIGN_BATCH_DETAILS drop column TEMPLATE_ID;

alter table CPN_CAMPAIGN_DL_BATCH add column TEMPLATE_ID CHAR(37);

alter table CPN_CAMPAIGN_DL_BATCH add column SENT_ON TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

CREATE INDEX DISTRIBUTION_INDEX ON CPN_DISTRIBUTION_LIST(email);

/*create index distribution_index on cpn_distribution_list  (email);*/

CREATE INDEX CAMPAIGN_INDEX ON CPN_DISTRIBUTION_LIST(campaign_id);

/*create index campaign_index on cpn_distribution_list  (campaign_id);*/


update SCT_APP_INFO set APP_VERSION ='1.0.0.1';
