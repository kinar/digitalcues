<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="navbar navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#"><spring:message code="label.master.logo"/></a>

        </div>
    </div>
</div>


<div class="container">
    <c:if test="${not empty form.feedback}">
    <span class="alert alert-success span10">
            ${form.feedback}
    </span>
    </c:if>
    <c:if test="${not empty form.exception}">
    <span class="alert alert-error span10">
            ${form.exception.message}
    </span>
    </c:if>
<div class="row">&nbsp;</div>
